# coding=utf-8
from __future__ import unicode_literals
import os

from fabric.operations import run
from git import Repo

from common.utils import setup_host
import settings


def local(branch_name):
    for project_name, project in settings.PROJECTS.iteritems():
        if not project['local_path']:
            continue
        if project['local_path'] == os.getcwd():
            continue
        repo = Repo(project['local_path'])
        assert not repo.bare
        current_branch = repo.active_branch
        response = repo.git.stash('save')
        stashed = current_branch.name in response
        repo.heads[branch_name].checkout()
        repo.remotes.origin.pull()

        for submodule in repo.submodules:
            submodule_repo = submodule.module()
            submodule_repo.remotes.origin.pull()
            repo.git.add(submodule.path)

        repo.index.commit('update submodules')
        ref = repo.refs[branch_name]
        repo.remotes.origin.push(ref)

        current_branch.checkout()
        if stashed:
            repo.git.stash('pop')


def remote(project_name):
    setup_host(settings.PROJECTS[project_name])
    log_file = os.path.join(settings.LOGS_DIR, '{}-update'.format(project_name))
    with open(log_file, 'w') as f:
        run('project_update', stderr=f, stdout=f)

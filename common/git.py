# coding=utf-8
from __future__ import unicode_literals
from fabric.context_managers import settings, cd
from fabric.contrib.console import confirm
from fabric.operations import run, os
from fabric.contrib import files

__all__ = ['RemoteGit']


class Submodule(object):
    def __init__(self, path):
        self.path = path

    def __getattr__(self, item):
        if item in ('init', 'update',):
            def f():
                with cd(self.path):
                    run('git submodule {}'.format(item))
            return f
        else:
            return super(Submodule, self).__getattr__(item)


class RemoteGit(object):
    def __init__(self, path, verbose=True):
        self.path = path
        self.verbose = verbose
        if not files.exists(path):
            run('mkdir -p {}'.format(path))
        self.submodule = Submodule(path)

    def clone(self, url):
        with settings(warn_only=True):
            with cd(self.path):
                result = run('git clone {} .'.format(url))

            if result.failed:
                msg = result.replace('\r', '').replace('\n', '')

                if 'already exists and is not an empty dir' in msg:
                    if self._confirm('{}\nu want delete all files from path {} ?'.format(msg, self.path)):
                        run('rm -rf {}/*'.format(self.path))
                        run('rm -rf {}/.*'.format(self.path))
                        self.clone(url)
                    else:
                        if not self._confirm('continue ?'):
                            raise SystemExit

                elif 'Repository does not exist' in msg:
                    if not self._confirm('{}\ncontinue ?'.format(msg)):
                        raise SystemExit

    def _confirm(self, msg):
        return confirm(msg) if self.verbose else True

    @property
    def has_sources(self):
        return files.exists(os.path.join(self.path, '.git'))

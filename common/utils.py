# coding=utf-8
from __future__ import unicode_literals
from fabric.state import env


def setup_host(server):
    env.host_string = '{user}@{host}:{port}'.format(
        user=server['sudo_user'],
        host=server['host'],
        port=server['port']
    )

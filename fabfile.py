# coding=utf-8
from __future__ import unicode_literals

from multiprocessing import Pool

from fabric.operations import os

import deploy
import settings
import update


def local_update(branch_name='master'):
    update.local(branch_name)


def remote_update():
    if not os.path.exists(settings.LOGS_DIR):
        os.makedirs(settings.LOGS_DIR)
    pool = Pool()
    pool.map(update.remote, settings.PROJECTS)


def deploy_project(project_name):
    deploy.new_project(project_name)


def deploy_all_projects():
    pool = Pool()
    pool.map(deploy_project, settings.PROJECTS)

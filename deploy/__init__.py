# coding=utf-8
from __future__ import unicode_literals

from common.utils import setup_host
import settings


def new_project(project_name):
    project = settings.PROJECTS[project_name]
    setup_host(project)
    for task in project['deploy']:
        task.run(project_settings=project, project_name=project_name)

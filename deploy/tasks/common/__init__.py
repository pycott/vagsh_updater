# coding=utf-8
from __future__ import unicode_literals
import os

from fabric.operations import run

from common import venv
from common.git import RemoteGit


class MakeEnv(object):
    def __init__(self, postfix_path):
        self.postfix_path = postfix_path

    def run(self, *args, **kwargs):
        venv_path = os.path.join(kwargs['project_settings']['remote_path'], self.postfix_path)
        venv.make_virtualenv(venv_path)


class GitCloner(object):
    def __init__(self, repos):
        self.repos = repos

    def run(self, *args, **kwargs):
        for repo_postfix_dir, repo_url in self.repos.iteritems():
            repo = RemoteGit(os.path.join(kwargs['project_settings']['remote_path'], repo_postfix_dir), verbose=False)
            repo.clone(repo_url)
            if repo.has_sources:
                repo.submodule.init()
                repo.submodule.update()


class BashScript(object):
    def __init__(self, path_script):
        self.path_script = path_script

    def run(self, *args, **kwargs):
        run('{} {}'.format(self.path_script, kwargs['project_name']))

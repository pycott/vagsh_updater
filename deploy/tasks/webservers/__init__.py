# coding=utf-8
from __future__ import unicode_literals

from fabric.contrib.files import upload_template, is_link, append
from fabric.operations import run


class Apache2Vhost(object):
    vhost_template = 'vhost.conf'
    vhost_destination = '/etc/apache2/sites-available/{}.conf'
    vhost_symlink = '/etc/apache2/sites-enabled/{}.conf'
    proxies_conf = '/etc/apache2/sites-available/proxies.conf'

    def __init__(self, proxy_port, prefix_url):
        self.proxy_port = proxy_port
        self.prefix_url = prefix_url

    def run(self, *args, **kwargs):
        project_name = kwargs['project_name']
        self.gen_vhost_conf(project_name)
        self.change_proxy_conf(project_name)

    def gen_vhost_conf(self, project_name):
        from settings import TEMPLATE_DIR
        destination = self.vhost_destination.format(project_name)
        symlink = self.vhost_symlink.format(project_name)
        context = {'port': self.proxy_port, 'project_name': project_name}
        upload_template(self.vhost_template, destination, context=context, template_dir=TEMPLATE_DIR, use_jinja=True)
        if not is_link(symlink):
            run('ln -s {} {}'.format(destination, symlink))

    def change_proxy_conf(self, project_name):
        text = (
            '\nProxyPass /{} http://localhost:{}'.format(self.prefix_url, self.proxy_port),
            'ProxyPassReverse /{} http://localhost:{}'.format(self.prefix_url, self.proxy_port),
        )
        append(self.proxies_conf, text, partial=True)

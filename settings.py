# coding=utf-8
from __future__ import unicode_literals
import os
from deploy.tasks.common import MakeEnv, BashScript
from deploy.tasks.common import GitCloner
from deploy.tasks.webservers import Apache2Vhost

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
LOGS_DIR = os.path.join(BASE_DIR, 'logs')
TEMPLATE_DIR = os.path.join(BASE_DIR, 'config_templates')


PROJECTS = {
    'admin-module': {
        'port': 22,
        'host': 'kpa.vagshdev.local',
        'sudo_user': 'root',
        'local_path': '/Users/buffagon/Projects/VAGSH/AdminServer/admin_server',
        'remote_path': '/home/projects/admin-module',
        'deploy': (
            MakeEnv('env'),
            GitCloner({
                'sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/admin_server.git',
                'client_sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/admin_client.git',
            }),
            Apache2Vhost(8001, 'admin'),
            BashScript('/home/bamboo_scripts/after_update.sh'),
        ),
    },
    'bpmn-module': {
        'port': 22,
        'host': 'kpa.vagshdev.local',
        'sudo_user': 'root',
        'local_path': '/Users/buffagon/Projects/VAGSH/BpmnServer/bpmn_server',
        'remote_path': '/home/projects/bpmn-module',
        'deploy': (
            MakeEnv('env'),
            GitCloner({
                'sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/bpmn_server.git',
                'client_sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/bpmn_client.git',
            }),
            Apache2Vhost(8002, 'bpmn'),
            BashScript('/home/bamboo_scripts/after_update.sh'),
        ),
    },
    'classifications-module': {
        'port': 22,
        'host': 'kpa.vagshdev.local',
        'sudo_user': 'root',
        'local_path': '/Users/buffagon/Projects/VAGSH/ClassificationsServer/classifications_server',
        'remote_path': '/home/projects/classifications-module',
        'deploy': (
            MakeEnv('env'),
            GitCloner({
                'sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/classifications_server.git',
                'client_sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/classifications_client.git',
            }),
            Apache2Vhost(8003, 'classifications'),
            BashScript('/home/bamboo_scripts/after_update.sh'),
        ),
    },
    'contacts-module': {
        'port': 22,
        'host': 'kpa.vagshdev.local',
        'sudo_user': 'root',
        'local_path': '/Users/buffagon/Projects/VAGSH/ContactsServer/contacts_server',
        'remote_path': '/home/projects/contacts-module',
        'deploy': (
            MakeEnv('env'),
            GitCloner({
                'sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/contacts_server.git',
                'client_sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/contacts_client.git',
            }),
            Apache2Vhost(8004, 'contacts'),
            BashScript('/home/bamboo_scripts/after_update.sh'),
        ),
    },
    'hr-module': {
        'port': 22,
        'host': 'kpa.vagshdev.local',
        'sudo_user': 'root',
        'local_path': '/Users/buffagon/Projects/VAGSH/HRServer/hr_server',
        'remote_path': '/home/projects/hr-module',
        'deploy': (
            MakeEnv('env'),
            GitCloner({
                'sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/hr_server.git',
                'client_sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/client.git',
            }),
            Apache2Vhost(8005, 'hr'),
            BashScript('/home/bamboo_scripts/after_update.sh'),
        ),
    },
    'location-module': {
        'port': 22,
        'host': 'kpa.vagshdev.local',
        'sudo_user': 'root',
        'local_path': '/Users/buffagon/Projects/VAGSH/HRServer/hr_server',
        'remote_path': '/home/projects/location-module',
        'deploy': (
            MakeEnv('env'),
            GitCloner({
                'sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/location_server.git',
                'client_sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/location_client.git',
            }),
            Apache2Vhost(8006, 'location'),
            BashScript('/home/bamboo_scripts/after_update.sh'),
        ),
    },
    'dashboard-module': {
        'port': 22,
        'host': 'kpa.vagsh.local',
        'sudo_user': 'root',
        'local_path': '/Users/buffagon/Projects/VAGSH/HRServer/hr_server',
        'remote_path': '/home/projects/dashboard-module',
        'deploy': (
            MakeEnv('env'),
            GitCloner({
                'sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/dashboard_server.git',
                'client_sources': 'ssh://git@stash.crvtu.ru:7999/vagsh/dashboard_server.git',
            }),
            Apache2Vhost(8007, 'dashboard'),
            BashScript('/home/bamboo_scripts/after_update.sh'),
        ),
    },
}

try:
    from local_settings import *
except ImportError:
    pass
